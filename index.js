'use strict'
const changeTheme = document.querySelector('.theme-changer');
const body = document.querySelector('body');

if (!localStorage.getItem('theme') || localStorage.getItem('theme') === 'orange'){
    localStorage.setItem('theme', 'orange');
    body.dataset.theme = 'orange';
    changeTheme.textContent = 'Orange';
}
if(localStorage.getItem('theme') === 'green'){
    body.dataset.theme = 'green';
    changeTheme.textContent = 'Green';
}


changeTheme.addEventListener('click', themeChanger);
function themeChanger(){
        if (body.dataset.theme === 'orange'){
            body.dataset.theme = 'green';
            changeTheme.textContent = 'Green';
            localStorage.setItem('theme', 'green');
    } else{ body.dataset.theme = 'orange';
            changeTheme.textContent = 'Orange';
            localStorage.setItem('theme', 'orange')
        }
}